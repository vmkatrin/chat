//
//  AuthError.swift
//  IChat
//
//  Created by Katrin on 05.06.2021.
//

import Foundation

enum AuthError {
    case notFilled
    case invalidEmail
    case passwordNorMatched
    case unknownError
    case serverError
}

extension AuthError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("All fields must be filled", comment: "")
        case .invalidEmail:
            return NSLocalizedString("The mail format is not valid", comment: "")
        case .passwordNorMatched:
            return NSLocalizedString("Password mismatch", comment: "")
        case .unknownError:
            return NSLocalizedString("Unknown error", comment: "")
        case .serverError:
            return NSLocalizedString("Server error", comment: "")
        }
    }
}
