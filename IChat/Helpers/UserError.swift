//
//  UserError.swift
//  IChat
//
//  Created by Katrin on 05.06.2021.
//

import Foundation

enum UserError {
    case notFilled
    case photoNotExist
    case cannotUnwrapToMUser
    case cannotGetUserInfo
}

extension UserError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("All fields must be filled", comment: "")
        case .photoNotExist:
            return NSLocalizedString("User did not select a photo", comment: "")
        case .cannotGetUserInfo:
            return NSLocalizedString("Unable to load user information from Firebase", comment: "")
        case .cannotUnwrapToMUser:
            return NSLocalizedString("Unable to convert MUser information from User", comment: "")
        }
    }
}

