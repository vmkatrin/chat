//
//  SelfConfiguringCell.swift
//  IChat
//
//  Created by Katrin on 03.06.2021.
//

import Foundation

protocol SelfConfiguringCell {
    
    static var reuseId: String { get }
    func configure<U: Hashable>(with value: U)
}
