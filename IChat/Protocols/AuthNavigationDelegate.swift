//
//  AuthNavigationDelegate.swift
//  IChat
//
//  Created by Katrin on 05.06.2021.
//

import Foundation

// для закрытия экрана и открытия нового по тапу на кнопку
protocol AuthNavigationDelegate: class {
    func toLogincVC()
    func toSignUpVC()
}
