//
//  WaitingChatsNavigation.swift
//  IChat
//
//  Created by Katrin on 06.06.2021.
//

import Foundation

protocol WaitingChatsNavigation: class {
    func removeWaitingChat(chat: MChat)
    func changeToActive(chat: MChat)
}
