//
//  Label + Extension.swift
//  IChat
//
//  Created by Katrin on 31.05.2021.
//

import Foundation
import UIKit

extension UILabel {
    
    convenience init(text: String, font : UIFont? = .avenir20()) {
        self.init()
        
        self.text = text
        self.font = font
    }
}
